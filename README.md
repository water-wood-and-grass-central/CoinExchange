
你的 ⭐️ Star ⭐️，是作者生发的动力！

**严肃声明：在使用本系统时，使用方必须在国家相关法律法规范围内并经过国家相关部门的授权许可，禁止用于一切非法行为。**

> 之前有哥们反应编译过程缺少spark-core-2.6.0.jar，已补充
>

## 🐶 商业版本演示 ...

[点我直达 → PC电脑端](https://pc.usdtest.online)

[点我直达 → H5手机版](https://mob.usdtest.online)

[点我直达 → 代理后台](https://agent.usdtest.online)

[点我直达 → 超级管理端](http://backed.usdtest.online)

#### 

## 🐯 平台简介

🔥 官方推荐 🔥 开源数字货币合约交易所，基于Java开发的BTC交易所 | ETH交易所撮合交易引擎。本项目有完整的撮合交易引擎源码、后台管理（后端+前端）、前台（交易页面、活动页面、个人中心等）、安卓APP源码、苹果APP源码、币种钱包RPC源码。

技术微信【**btxchain**】

* 管理后台的电脑端：Vue2 版本
* 最新开发的混合式APP：采用 [uni-app](https://github.com/dcloudio/uni-app) 方案，一份代码多终端适配，同时支持 APP、H5！（未开源）
* 后端：Spring、SpringMVC、SpringData、SpringCloud、SpringBoot
* 数据库：Mysql、Mongodb
* 原生App：Java和ObjectC提供原生的安卓和苹果APP体验
* 撮合引擎：交易队列采用内存撮合的方式进行
* 权限认证使用 Spring Security & Token & Redis，支持多终端、多种用户的认证系统
* 支持加载动态权限菜单，按钮级别权限控制，本地缓存提升性能
* 支持 SaaS 多租户，可自定义每个租户的权限，提供透明化的多租户底层封装
* 集成阿里云短信、阿里邮件推送渠道，集成阿里云存储服务

##  🐳 项目关系

![整体架构](/.image/common/1zhengti.png)

![逻辑架构图](/.image/common/2luoji.png)

![部署架构图](/.image/common/3bushu.png)

![依赖架构图](/.image/common/4yilai.png)

### 后端项目


| 项目                | 简介                              |
| ------------------- | --------------------------------- |
| admin               | 后台管理服务接口                  |
| agent-api           | 基于 Spring Cloud 微服务架构      |
| chat                | 系统学习 Spring Boot & Cloud 专栏 |
| contract-option-api | 期权合约服务接口                  |
| contract-swap-api   | 永续合约服务接口                  |
| contract-second-api | 秒合约服务接口（未开源）          |
| exchange            | 撮合交易引擎                      |
| otc-api             | OTC交易服务接口                   |
| ucenter-api         | 会员中心                          |
| wallet              | 钱包服务统一接口                  |
| wallet_udun         | 优盾钱包服务                      |
| market              | 行情服务                          |

### 行情Robot项目


| 项目            | 简介                                 |
| --------------- | ------------------------------------ |
| er_market       | 外部行情获取引擎，获取外部价格工程   |
| er_robot_normal | 一般机器人（外部行情有价格的交易对） |
| er_robot_price  | 恒定价格机器人                       |
| er_robot_custom | 控pAn机器人，自定义调整行情k线       |

### 前端项目

| 项目      | 简介                         |
| --------- | ---------------------------- |
| Web_Admin | 管理后台PC                   |
| Web_Agent | 代理商管理后台PC             |
| Web       | PC用户端                     |
| uniapp    | 基于 uni-app 实现的混合式APP |



## 😎 开源协议

① 本项目采用比 Apache 2.0 更宽松的 [MIT License]开源协议，个人与企业可 100% 免费使用，不用保留类作者、Copyright 信息。

② 代码全部开源，不会像其他项目一样，只开源部分代码，让你无法了解整个项目的架构设计。

### 框架

| 框架                                                         | 说明             | 版本           | 学习指南 |
| ------------------------------------------------------------ | ---------------- | -------------- | -------- |
| [Spring Boot](https://spring.io/projects/spring-boot)        | 应用开发框架     | 2.7.16         |          |
| [MySQL](https://www.mysql.com/cn/)                           | 数据库服务器     | 5.7            |          |
| [Redis](https://redis.io/)                                   | key-value 数据库 | 5.0 / 6.0 /7.0 |          |
| [Spring MVC](https://github.com/spring-projects/spring-framework/tree/master/spring-webmvc) | MVC 框架         | 5.3.24         |          |

## 🐷 演示图

### 系统功能

| 模块        | biu                          | biu                           | biu                         |
| ----------- | ---------------------------- | ----------------------------- | --------------------------- |
| 登录 & 首页 | ![首页](/.image/index01.png) | ![交易](/.image/exchange.png) | ![个人中心](/.image/uc.png) |
